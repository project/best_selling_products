## INTRODUCTION

"Best selling products" main feature of this module is the possibility to build
and display "the best sales" block with products in your shop pages.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/best_selling_products

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/best_selling_products


## INSTALLATION

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

 * After installation, there will be "The best sales" block with products on
   shop pages.


## REQUIREMENTS

 * This module requires no additional modules outside of Drupal Core.
 * [Drupal Commerce]:
   Drupal Commerce is used to build eCommerce websites and applications of all
   sizes. At its core it is lean and mean, enforcing strict development
   standards and leveraging the greatest features of Drupal 7 and major modules
   like Views and Rules for maximum flexibility.


## CONFIGURATION

 * No configuration is needed.
 * Recommend to clear Drupal cache.
 * Prior to usage, we recommend clearing the Drupal cache.


## MAINTAINERS

Current maintainers:

 * Artem Sosiedko (https://www.drupal.org/u/artsays)
   He can be contacted for work on this module or other custom projects.

[Drupal Commerce]: https://www.drupal.org/project/commerce
