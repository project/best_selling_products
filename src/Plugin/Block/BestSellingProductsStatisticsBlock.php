<?php

namespace Drupal\best_selling_products\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\best_selling_products\Service\ProductsServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Best Selling Products Statistics Block.
 *
 * @Block(
 *   id = "best_selling_products_statistics_block",
 *   admin_label = @Translation("Best Selling Products Statistics"),
 *   category = @Translation("Statistics"),
 * )
 */
class BestSellingProductsStatisticsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Default number of products to display.
   */
  const DEFAULT_NUMBER_OF_PRODUCTS = 5;

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The products service.
   *
   * @var \Drupal\best_selling_products\Service\ProductsServiceInterface
   */
  protected $productsService;

  /**
   * Constructs a BestSellingProductsStatisticsBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\best_selling_products\Service\ProductsServiceInterface $products_service
   *   The products service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    ProductsServiceInterface $products_service,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->productsService = $products_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('best_selling_products.products')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'number_of_products' => self::DEFAULT_NUMBER_OF_PRODUCTS,
      'max_age' => 86400,
      'bundle' => 'all',
      'store' => 'all',
      'strict_sequences' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $number_of_products = (int) $this->configuration['number_of_products'];
    $bundle = $this->configuration['bundle'];
    $strict_sequences = $this->configuration['strict_sequences'];
    $store = $this->configuration['store'];

    // Fetch best-selling products using the products service.
    $products = $this->productsService->bestSellingProducts($number_of_products, $bundle, $strict_sequences, $store);

    if ($products) {
      // Define table headers.
      $header = [
        $this->t('Product ID'),
        $this->t('Title'),
        $this->t('Sales Count'),
        $this->t('Link to Product'),
        // Add more headers as needed.
      ];

      // Generate table rows.
      $rows = $this->getTableRows($products);

      // Build the render array for the table.
      $build = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => ['class' => ['best-selling-products-table']],
        '#cache' => [
          'max-age' => ($this->configuration['max_age'] == -1) ? Cache::PERMANENT : (int) $this->configuration['max_age'],
        ],
      ];

      // Optionally, attach a library for styling.
      $build['#attached']['library'][] = 'best_selling_products/best_selling_products_styles';

      return $build;
    }
    else {
      // Display a message if no products are found.
      return [
        '#markup' => $this->t('No best-selling products found.'),
      ];
    }
  }

  /**
   * Generates table rows from product entities.
   *
   * @param array $products
   *   An array of product entities.
   *
   * @return array
   *   An array of table rows.
   */
  protected function getTableRows(array $products) {
    $rows = [];

    foreach ($products as $product) {
      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      $product_id = $product->id();
      $title = $product->getTitle();
      // Ensure 'sales_count' is available.
      $sales_count = $product->sales_count ?? 0;
      // Create a link to the product page.
      $url = Url::fromRoute('entity.commerce_product.canonical', ['commerce_product' => $product_id]);

      $rows[] = [
        'data' => [
          'product_id' => $product_id,
          'title' => $title,
          'sales_count' => $sales_count,
          'product_link' => [
            'data' => [
              '#type' => 'link',
              '#title' => $this->t('View Product'),
              '#url' => $url,
              '#attributes' => ['class' => ['button', 'button--small']],
            ],
          ],
        ],
      ];
    }

    return $rows;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // Number of products to display.
    $form['number_of_products'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of products to display'),
      '#description' => $this->t('Specify how many best-selling products to display in the table.'),
      '#default_value' => $config['number_of_products'],
      '#min' => 1,
      '#max' => 100,
      '#required' => TRUE,
    ];

    // Select Store.
    $store_query = $this->entityTypeManager->getStorage('commerce_store')->getQuery()->accessCheck(TRUE);
    $store_ids = $store_query->execute();
    if (!empty($store_ids)) {
      $stores = $this->entityTypeManager->getStorage('commerce_store')->loadMultiple($store_ids);
      $store_options = ["all" => $this->t('All Stores')];
      foreach ($stores as $store) {
        $store_options[$store->id()] = $store->getName();
      }
      $form['store'] = [
        '#type' => 'select',
        '#title' => $this->t('Select Store'),
        '#options' => $store_options,
        '#default_value' => $config['store'] ?? 'all',
      ];
    }

    // Select Product Bundle.
    $product_bundles = $this->entityTypeBundleInfo->getBundleInfo('commerce_product');
    if (!empty($product_bundles)) {
      $bundles = ["all" => $this->t('All Bundles')];
      foreach ($product_bundles as $key => $bundle) {
        $bundles[$key] = $bundle['label'];
      }

      $form['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Select Product Bundle'),
        '#options' => $bundles,
        '#default_value' => $config['bundle'] ?? 'all',
      ];
    }

    // Cache settings.
    $form['max_age'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache Lifetime'),
      '#description' => $this->t('Set how long the block should be cached.'),
      '#options' => [
        '0' => $this->t('No caching'),
        '1800' => $this->t('30 minutes'),
        '3600' => $this->t('1 hour'),
        '21600' => $this->t('6 hours'),
        '43200' => $this->t('12 hours'),
        '86400' => $this->t('1 day'),
        '172800' => $this->t('2 days'),
        '432000' => $this->t('5 days'),
        '604800' => $this->t('1 week'),
        '-1' => $this->t('Permanent'),
      ],
      '#default_value' => $config['max_age'] ?? '86400',
    ];
    $form['strict_sequences'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strict sequences of products'),
      '#default_value' => $this->configuration['strict_sequences'] ?? FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->configuration['number_of_products'] = $form_state->getValue('number_of_products');
    $this->configuration['store'] = $form_state->getValue('store') ?? 'all';
    $this->configuration['bundle'] = $form_state->getValue('bundle') ?? 'all';
    $this->configuration['max_age'] = $form_state->getValue('max_age');
    $this->configuration['strict_sequences'] = $form_state->getValue('strict_sequences');
  }

}
