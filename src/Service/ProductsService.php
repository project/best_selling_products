<?php

namespace Drupal\best_selling_products\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\mysql\Driver\Database\mysql\Connection;

/**
 * This class implements Products Service.
 */
class ProductsService implements ProductsServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\mysql\Driver\Database\mysql\Connectionn definition.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  /**
   * Constructs a new ProductService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function bestSellingProducts($number_of_products, $bundle, $strict_sequences, $store) {
    $query = $this->database->select('commerce_order', 'co')
      ->fields('pvfd', ['product_id']);
    $count_field = $query->addExpression('COUNT(pvfd.product_id)', 'count');
    $query->leftJoin('commerce_order_item', 'coi', 'co.order_id=coi.order_id');
    $query->leftJoin('commerce_product_variation_field_data', 'pvfd', 'coi.purchased_entity=pvfd.variation_id');
    $query->condition('co.state', 'completed');
    if (!empty($store) && $store != 'all') {
      $query->condition('store_id', $store);
    }
    $query->isNotNull('pvfd.product_id');
    $query->groupBy('pvfd.product_id');
    $query->orderBy($count_field, 'DESC');
    if ($strict_sequences) {
      $query->orderBy('pvfd.product_id', 'DESC');
    }
    $query->addTag('best_selling_products');
    $result = $query->execute()->fetchAll();

    $product_ids = [];
    $sales_counts = [];
    foreach ($result as $record) {
      $product_ids[] = $record->product_id;
      $sales_counts[$record->product_id] = $record->count;
    }

    $products = $this->isPublishedProducts($product_ids, $number_of_products, $bundle);
    // Attach sales count to products.
    foreach ($products as $product) {
      $product->sales_count = $sales_counts[$product->id()] ?? 0;
    }

    return $products;
  }

  /**
   * Get published products.
   */
  private function isPublishedProducts($product_ids, $number_of_products, $bundle) {
    $products = [];
    foreach ($product_ids as $product_id) {
      /** @var \Drupal\commerce_order\Entity\Order $order */
      $product = $this->entityTypeManager->getStorage('commerce_product')
        ->load($product_id);

      if ($product instanceof ProductInterface && $product->isPublished() && ($product->bundle() === $bundle || $bundle === 'all')) {
        $products[$product->id()] = $product;
      }
      if (count($products) == $number_of_products) {
        break;
      }
    }
    return $products;
  }

}
